#!/usr/bin/env python
from __future__ import annotations

import argparse
import dataclasses
import enum
import json
import pathlib
import pkg_resources
from typing import Optional, Dict, Union, List

from hardehar import har, util

class Action(list, enum.Enum):
    REQUEST = ['request']
    RESPONSE = ['response']
    BOTH = ['request', 'response']

    @classmethod
    def from_str(cls, key: str) -> Action:
        return getattr(cls, key.upper())

class Command:
    pass


class DictEncoder(json.JSONEncoder):
    def default(self, obj):
        if hasattr(obj, '_as_dict'):
            return obj._as_dict()
        if isinstance(obj, set):
            return list(obj)
        super().default(obj)

def find_plugin_commands() -> List:
    cmds = { entry_point.name: entry_point.load()
             for entry_point
             in pkg_resources.iter_entry_points('hardehar.commands') }
    return cmds
    
def toggle(p: argparse.ArgumentParser,
           short: str,
           long: str,
           help: str,
           default: bool = True,
           dest: Optional[str]=None,
           metavar: Optional[str]=None) -> argparse._MutuallyExclusiveGroup:
    """Convenience function to create -opt/+opt command-line toggles

    :param p: the root argument parser instance
    :param short: the short switch name
    :param long: the long switch name
    :param help: a declarative help sentence
    :param default: whether the default should be True or False
    :param dest: optional - same as
        :meth:`argparse.ArgumentParser.add_argument`
    :param metavar: optional - same as
        :meth:`argparse.ArgumentParser.add_argument`
    
    Short and long name strings should not include any prefix characters (-,
    --, or +). Switch names will be automatically generated in the form
    "-s/+s" for the short switch, and "--long/--no-long" for the long
    switch. Likewise help strings should be declarative ("make foo a baz") so
    that the automatically generated help ("make foo a baz"/"do not make foo a
    baz") makes sense.
    """
    mv = {'metavar': metavar} if metavar is not None else {}
    if dest is not None:
        mv['dest'] = dest
    (ht, hf) = ('[default]', '') if default else ('', '[default]')
    g = p.add_mutually_exclusive_group()
    g.add_argument(f'+{short}', f'--{long}', help=f'{help} {ht}',
                   action='store_true', default=default, **mv)
    g.add_argument(f'-{short}', f'--no-{long}', help=f'do not {help} {hf}',
                   action='store_false', default=default, **mv)
    return g

def parse_cmdline(p: argparse.ArgumentParser,
                  commands: list=None) -> argparse.Namespace:
    req = p.add_argument_group('required named arguments')
    req.add_argument('-f', '--file', type=pathlib.Path, metavar='LOG_FILE',
                   help='JSON HAR log file path', required=True)
    subp = p.add_subparsers(help='command help')
    for name, cmd in commands.items():
        cmd = cmd() # Instantiate first to allow for @properties
        cname = getattr(cmd, 'command_name', name)
        ds = cmd.__class__.__doc__
        chelp = getattr(cmd, 'help', ds if ds else 'no help available')
        s = subp.add_parser(cname, prefix_chars='-+', help=chelp)
        s.set_defaults(cmd=cmd, **cmd.cmdline_args(s))
    args = p.parse_args()
    return args

def main():
    commands = find_plugin_commands()
    parser = argparse.ArgumentParser(description="Process HAR log files")
    args = parse_cmdline(parser, commands)

    harlog = har.HAR.load(args.file)
    res = args.cmd(harlog, args)
    #rdict = { k: { 'count': res[k]['count'],
    #               'instances': [vars(i) for i in res[k]['hits']] }
    #          for k, v in res.items() }
    print(json.dumps(res, cls=DictEncoder, indent=2))
            
    
if __name__ == '__main__':
    main()
