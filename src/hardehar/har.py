"""Load HAR file contents as Python objects, with type conversion

This module is essentially "ORM for HAR data". It parses the JSON HAR data
into a tree of Python dataclasses, whose attributes are the data values from
the log. This allows simpler use of, e.g., Python's itertools and other
language features to examine and manipulate the log data.

In addition, it "type-casts" specific data into Python types where applicable,
to allow the full array of tools for those types to be readily
used. E.g. times and dates are converted to :class:`datetime.datetime`
objects, HTML is parsed, IP addresses become :class:`ipaddress.IPv4Address`
objects, etc.
"""

from __future__ import annotations
import dataclasses
import datetime
import decimal
import functools
import http
import ipaddress
import json
import pathlib
import typing
# Localilze some :mod:`typing` locals to save... typing.
from typing import (Optional, Any, Callable, Dict, Sequence, Mapping, List,
                    Tuple,)
import urllib.parse

from dateutil import parser as duparser

from . import util


#decimal.getcontext().prec = 9

# N.B. lxml/ElementTree parsing not tested
try:
    import bs4
    _parse_html = functools.partial(bs4.BeautifulSoup,
                                     features='html.parser')
    HTMLType = bs4.BeautifulSoup
except ModuleNotFoundError:
    try:
        from lxml import etree
        _parser = etree.HTMLParser
        _parse_html = functools.partial(etree.parse, parser=_parser)
        HTMLType = etree.ElementTree
    except ModuleNotFoundError:
        import xml.etree.ElementTree as ET
        _parse_html = ET.parse
        HTMLType = ET

# Not present before 3.8 - fake it.
type_args = getattr(typing, 'get_args', lambda x: getattr(x, '__args__',
                                                          (x,)))
type_origin = getattr(typing, 'get_origin', lambda x: getattr(x, '__origin__',
                                                              x))

def typed_field(caster: Optional[Callable]=None,
                preferred_type: Optional[Callable]=None,
                json_revert: Optional[Callable]=None,
                default: util.OptVoid[Any]=util.Void,
                default_factory: util.OptVoid[Callable]=util.Void,
                init: util.OptVoid[bool]=util.Void,
                repr: util.OptVoid[bool]=util.Void,
                compare: util.OptVoid[bool]=util.Void,
                hash: util.OptVoid[bool]=util.Void, 
                metadata: util.OptVoid[Mapping]=util.Void) -> dataclasses.Field:
    """ Return a dataclasses.field instance with typecasting metadata

    :param func caster: callable which accepts a value of some type and
        returns the same value as a given type (typecast)
    :param func preferred_type: if the type of a field can be one of multiple
        types (i.e. a Union or an Optional), this is the preferred type for
        arguments that need cast. Ignored if `caster` is provided.
    :return: a field instance with the metadata set to indicate this field
        should be typecast on dataclass instantiation
    :rtype: :class:`dataclass.Field`

    The metadata key is set to |__name__| to allow for other plug-in usage as
    indicated in the dataclass documentation.  All arguments aside from
    `caster` and `preferred_type` are identical to their
    :func:`dataclasses.field` counterpart.

    This function works in conjunction with :class:`AutoTyped`.
    """
    args = locals()
    md = {}
    if type(args['metadata']) is util._Void:
        args['metadata'] = {}
    for arg in ['caster', 'preferred_type', 'json_revert']:
        v = args.pop(arg)
        if v is not None:
            md[arg] = v
    if __name__ in args['metadata']:
        md.update(args['metadata'][__name__])
    if md:
        args['metadata'][__name__] = md
    field = dataclasses.field(**{ k: v for k, v in args.items()
                                  if type(v) is not util._Void })
    return field

# Here be typecasting functions. Can probably be abstracted, but not today
def type_timestamp(string: str) -> datetime.datetime:
    """Convert a string to a :class:`datatime.datetime` object"""
    if string:
        return duparser.parse(string)
    return None

IPAddressType = typing.Union[ipaddress.IPv4Address, ipaddress.IPv6Address]
def type_ipaddress(string: str) -> IPAddressType:
    """Convert a string to an :class:`ipaddress.IPv4Address` (or v6)"""
    if string is not None:
        return ipaddress.ip_address(string)
    return string

def type_timedelta_ms(dstr: str) -> datetime.timedelta:
    """Convert a string, or numeric ms val to a :class:`datetime.timedelta`"""
    if int(float(dstr)) == -1:
        return datetime.timedelta(0) # Maybe return None? Something else?
    return datetime.timedelta(microseconds=float(dstr*1000))

def revert_timedelta_ms(delta: datetime.timedelta) -> float:
    (d, s, ms) = (decimal.Decimal(getattr(delta, a))
                  for a in [ 'days', 'seconds', 'microseconds' ])
    v = d*86400000 + s*1000 + ms/decimal.Decimal('1000')
    #v.quantize(decimal.Decimal('0.0001'), rounding=decimal.ROUND_HALF_UP)
    return v.normalize()

def type_timedelta_s(dstr: str) -> datetime.timedelta:
    """Like type_timedelta_ms but for second values"""
    if dstr is not None:
        return datetime.timedelta(seconds=float(dstr))
    return dstr

def revert_timedelta_s(delta: datetime.timedelta) -> float:
    return decimal.Decimal(delta.total_seconds())

def type_html(text: str) -> HTMLType:
    """Convert a string to an HTML object

    Prefers Beautiful Soup 4 if present (recommended), otherwise falls back to
    trying lxml if present, else reverting to the built-in ElementTree. lxml
    and particularly ElementTree may break with badly-formed HTML (read: most
    of the web), so installing Beautiful Soup 4 is strongly recommended.
    
    Alternately, you may monkey-patch the module function `_parse_html` with
    your own callable that accepts text input and produces the desired output.
    """
    return _parse_html(text)
    
def type_path(path: str) -> pathlib.PurePath:
    """Convert a string to a :class:`pathlib.PurePath` object"""
    if path is not None:
        path = pathlib.PurePath(path)
    return path


@dataclasses.dataclass
class HARObject():
    """Base class for all HAR objects"""
    _PARENT=None
    
    def as_dict(self):
        def revert(field):
            v = getattr(self, field.name)
            revert = field.metadata.get(__name__, {}).get('json_revert', None)
            return revert(v) if revert else v
        
        fields = dataclasses.fields(self)
        return { f.name: revert(f) for f in fields }

    def parent(self):
        return self._PARENT
    
@dataclasses.dataclass
class AutoTyped(HARObject):
    """Dataclass with self-typing attributes
    
    A base class or mixin for HAR dataclasses that uses the attribute type
    hints to do actual type-casting of the log data. This makes expanding the
    class structure relatively trivial, requiring little more than adding
    attributes with the appropriate types (or new dataclasses where
    necessary).

    Typing is typically done by passing the data value through an instance of
    the type class (e.g. int(5) or httpstatus.HTTPStatus(200)). For data types
    which cannot be cast using this method, like datetimes, html, etc., we
    make use of the :class:`dataclasses.Field` metadata mapping to provide a
    'caster' function which converts the data to the appropriate datatype -
    see the :func:`typed_field` function and the :class:`Entry` class for
    usage details and example usage.
    
    *Caveats*:
       - **This method performs evals.** Used solely to generate data types
         from their string representation, but the security implications
         should be kept in mind. It would be possible to replace this
         mechanism with some kind of lookup table, at the cost of added
         complexity overall. This does *not* evaluate any data from JSON.
       - Types (or casters) must be able to accept all possible data types
         that the JSON decoder may return for that key.
       - Optional types must be indicated as such using the
         :class:`~typing.Optional` type hint or some other
         :class:`~typing.Union`. 
       - 'Caster' functions are only passed the data value. No other
         information (such as sibling or parent data) is available to
         them. This is a limitation of the current implementation. However,
         constant value arguments may be handled by using the
         :func:`functools.partial` or other closure.
    """
    def __post_init__(self, *args, **kw):
        for f in dataclasses.fields(self):
            if f.name == '_PARENT': continue
            key = f.metadata.get(__name__, {})
            ptype = key.get('preferred_type', None)
            value = getattr(self, f.name)
            ftype = eval(f.type)
            args = type_args(ftype)
            origin = type_origin(ftype)
            tc_func = key.get('caster', None)
            #print(f'KEY: {key}  NAME: {f.name}  ORIGIN: {origin}  VAL: '
            #      f'{value!s:.10s} VTYPE: {type(value)} TCFUNC: {tc_func}')
            if type(value) in args:
                continue
            if origin is typing.Union:
                origin = ptype if ptype is not None else args[0]
            if issubclass(origin, typing.List):
                setattr(self, f.name,
                        _type_seq(ftype, value, preferred_type=ptype,
                                  caster=tc_func, parent=self))
            elif issubclass(origin, typing.Mapping):
                raise NotImplementedError('dataclass mapping autotype')
            else:
                if issubclass(origin, AutoTyped):
                    v = origin(**value)
                    v._PARENT = self
                    setattr(self, f.name, v)
                else:
                    if tc_func:
                        setattr(self, f.name, tc_func(value))
                    else:
                        setattr(self, f.name, origin(value))



def _type_seq(stype, data, preferred_type=None, caster=None, parent=None):
    """Given a sequence type hint, type-cast the sequence data

    Called by :class:`AutoTyped` to handle type hints like 'List[int]' by
    first converting all the values in the data sequence into, e.g. ints, then
    the sequence into a list which is returned.

    This function needs some love. At present there is no way to provide a
    'caster' for element types (although there is for the sequence type) aside
    from a kludge involving `preferred_type`. It also fails to handle things
    like lists of lists. Neither of which are present in the current HAL data
    structures, so back burner it is.
    """
    def cast(datum, typ, parent, datatypes):
        if type(datum) in datatypes:
            return datum
        if issubclass(typ, AutoTyped):
            v = typ(**datum)
            v._PARENT = parent
            return v
        return typ(datum)
    
    seq = caster if caster is not None else type_origin(stype)
    dtypes = type_args(stype)
    ctype = preferred_type if preferred_type is not None else dtypes[0]
    return seq([ cast(i, ctype, parent, dtypes) for i in data ])


@dataclasses.dataclass
class Entry(AutoTyped):
    """HAR `entry` container class"""

    pageref: str
    startedDateTime: datetime.datetime = typed_field(type_timestamp)
    time: datetime.timedelta = typed_field(caster=type_timedelta_ms,
                                           json_revert=revert_timedelta_ms)
    request: Request
    response: Response
    timings: Timings
    cache: Optional[Cache] = None
    serverIPAddress: Optional[IPAddressType] = typed_field(type_ipaddress,
                                                           default=None)
    headersSize: Optional[int] = None
    _serverPort: Optional[int] = None
    connection: Optional[int] = None # str?
    _fetchType: Optional[str] = None
    _priority: Optional[str] = None
    page: Optional[Page] = None
    comment: Optional[str] = None

    def as_dict(self):
        res = super().as_dict()
        res.pop('page')
        return res
    
@dataclasses.dataclass
class Page(AutoTyped):
    """HAR `page` container class"""
    id: str
    title: str
    pageTimings: PageTimings
    startedDateTime: datetime.datetime = typed_field(type_timestamp)
    entries: Tuple[Entry] = dataclasses.field(default_factory=tuple, repr=False)
    comment: Optional[str] = None

    def as_dict(self):
        res = super().as_dict()
        res.pop('entries')
        return res

@dataclasses.dataclass
class PageTimings(AutoTyped):
    """HAR `pageTimings` container class"""
    onContentLoad: datetime.timedelta = typed_field(caster=type_timedelta_ms,
                                                json_revert=revert_timedelta_ms)
    onLoad: datetime.timedelta = typed_field(type_timedelta_ms,
                                             json_revert=revert_timedelta_ms)
    comment: Optional[str] = None

    
@dataclasses.dataclass
class Timings(AutoTyped):
    """HAR `timings` container class"""
    blocked: datetime.timedelta = typed_field(type_timedelta_ms,
                                              json_revert=revert_timedelta_ms)
    dns: datetime.timedelta = typed_field(type_timedelta_ms,
                                          json_revert=revert_timedelta_ms)
    connect: datetime.timedelta = typed_field(type_timedelta_ms,
                                              json_revert=revert_timedelta_ms)
    ssl: datetime.timedelta = typed_field(type_timedelta_ms,
                                          json_revert=revert_timedelta_ms)
    send: datetime.timedelta = typed_field(type_timedelta_ms,
                                           json_revert=revert_timedelta_ms)
    wait: datetime.timedelta = typed_field(type_timedelta_ms,
                                           json_revert=revert_timedelta_ms)
    receive: datetime.timedelta = typed_field(type_timedelta_ms,
                                              json_revert=revert_timedelta_ms)
    comment: Optional[str] = None
    

@dataclasses.dataclass
class Content(AutoTyped):
    """HAR `content` container class"""
    size: int
    compression: int
    mimeType: str
    text: bs4.BeautifulSoup = typed_field(type_html, repr=False, default='')
    encoding: Optional[str] = None
    comment: Optional[str] = None

    
@dataclasses.dataclass
class Response(AutoTyped):
    """HAR `response` container class"""
    status: http.HTTPStatus = typed_field(http.HTTPStatus)
    statusText: str
    httpVersion: str
    cookies: List[Cookie]
    headers: List[Headers]
    headersSize: int 
    content: Content
    bodySize: int
    redirectURL: urllib.parse.SplitResult = typed_field(urllib.parse.urlsplit,
                                                     default=None)
    _transferSize: Optional[int] = None
    comment: Optional[str] = None


@dataclasses.dataclass
class Creator(AutoTyped):
    """HAR `creator` container class"""
    name: str
    version: str
    comment: Optional[str] = None

    
@dataclasses.dataclass
class HAR(AutoTyped):
    """HAR `log` container class"""
    entries: util.KeyedList[Entry]
    creator: Creator
    pages: util.KeyedList[Page]
    version: str = '1.1'
    browser: Optional[Creator] = None
    comment: Optional[str] = None

    @classmethod
    def load(cls, harpath):
        """Load a JSON HAR file, returning a populated HAR instance

        :param harpath: path to HAR log file
        :type harpath: :class:`pathlib.Path` or str
        """
        harpath = pathlib.Path(harpath)
        with harpath.open('r') as f:
            data = json.load(f, parse_float=decimal.Decimal)
            if 'log' not in data:
                raise ValueError(f'invalid HAR file "{harpath}"; '
                                 'does not contain a "log" key')
        return cls(**data['log'])

    def __post_init__(self):
        """Do some wiring"""
        super().__post_init__()
        for page in self.pages:
            page.entries = tuple(e for e in self.entries
                                 if e.pageref == page.id)
        for entry in self.entries:
            entry.page = self.pages.by_id[entry.pageref]

    def make_json(self) -> str:
        return json.dumps(self.as_dict(), cls=JSONize)

class JSONize(json.JSONEncoder):
    def default(self, o):
        if isinstance(o, datetime.datetime):
            return o.isoformat()
        elif isinstance(o, HARObject):
            return o.as_dict()
        elif isinstance(o, pathlib.PurePath):
            return str(o)
        elif isinstance(o, urllib.parse.SplitResult):
            return urllib.parse.urlunsplit(o)
        elif isinstance(o, (ipaddress.IPv4Address, ipaddress.IPv6Address)):
            return str(o)
        elif isinstance(o, bs4.BeautifulSoup):
            # str(o)?
            return ''
        else:
            return super().default(o)
    
@dataclasses.dataclass
class Cookie(AutoTyped):
    """HAR `cookie` container class"""
    name: str
    value: str
    httpOnly: bool = False
    path: Optional[pathlib.PurePath] = None
    domain: Optional[str] = None #URL?
    maxAge: Optional[datetime.timedelta] = typed_field(caster=type_timedelta_s,
                                                json_revert=revert_timedelta_s,
                                                default=None)
    expires: Optional[datetime.datetime] = typed_field(caster=type_timestamp,
                                                       default=None)
    secure: bool = False
    sameSite: Optional[str] = None # Enum (Lax, Strict, None)? non-standard?
    comment: Optional[str] = None

    
@dataclasses.dataclass
class PostData(AutoTyped):
    """HAR `postData` container class"""
    mimeType: Optional[str] = None
    params: List[Param] = dataclasses.field(default_factory=list)
    text: Optional[str] = None
    comment: Optional[str] = None


@dataclasses.dataclass
class Request(AutoTyped):
    """HAR `request` container class"""
    method: str
    url: urllib.parse.SplitResult = typed_field(urllib.parse.urlsplit)
    httpVersion: str
    cookies: List[Cookie]
    headers: List[Headers]
    queryString: List[QueryString]
    headersSize: int
    bodySize: int
    postData: PostData = dataclasses.field(default_factory=PostData)
    comment: Optional[str] = None


@dataclasses.dataclass
class Headers(AutoTyped):
    """HAR `headers` container class"""
    name: str
    value: str
    comment: Optional[str] = None


@dataclasses.dataclass
class QueryString(AutoTyped):
    """HAR `queryString` container class"""
    name: str
    value: str
    comment: Optional[str] = None

    
@dataclasses.dataclass
class Param(AutoTyped):
    """HAR `param` container class"""
    name: str
    value: str
    filename: pathlib.PurePath = typed_field(type_path, default=None)
    contentType: Optional[str] = None
    comment: Optional[str] = None

@dataclasses.dataclass
class Cache(AutoTyped):
    """HAR `cache` container class"""
    beforeRequest: Optional[CacheInfo] = None
    afterRequest: Optional[CacheInfo] = None

    
@dataclasses.dataclass
class CacheInfo(AutoTyped):
    """HAR `cacheInfo` container class"""
    lastAccess: datetime.datetime = typed_field(type_timestamp)
    expires: datetime.datetime = typed_field(type_timestamp, default=None)
    eTag: Optional[str] = None
    hitCount: int = 0
    comment: Optional[str] = None


testdata = pathlib.Path('/home/bslabs/Projects/Code/python/virtual/hardehar/src/snippets/vinylmoon.co.har')

__all__ = ['typed_field', 'type_timestamp', 'type_ipaddress',
           'type_timedelta_ms', 'type_timedelta_s', 'type_html', 'type_path',
           'type_keyed_list', 'HARObject', 'AutoTyped', 'HAR', 'Entry',
           'Page', 'PageTimings', 'Timings', 'Content', 'Response', 'Creator',
           'Cookie', 'PostData', 'Request', 'Headers', 'QueryString', 'Param',
           'Cache', 'CacheInfo',]
           
