"""Various utility classes and functions for HARdeHAR"""
import collections
import collections.abc
import re
import typing
from typing import List, Iterable, Hashable, TypeVar, Any, Union


# Argument default value for optional arg signatures to differientiate
# between Any and no value provided.
# FIXME: Clean this up. Major redundancy and silliness here.
_Void = type('_Void', (), {})
Void = _Void()
VoidType = typing.NewType('VoidType', _Void)
_VT = typing.TypeVar('_VT')
OptVoid = typing.Union[VoidType, _VT]

IterVal = TypeVar('IterVal')
OneOrMany = Union[List[typing.T], typing.T]

class MultiValueDict(collections.abc.MutableMapping):
    """A very simple implementation of an identical key dictionary

    Implements a collection class that may have multiple values for the same
    key. All values are stored as lists, even if a given key only pairs with a
    single value.
    """
    def __init__(self, ivals = []):
        self._data = {}
        if isinstance(ivals, dict):
            ivals = ivals.items()
        for k, v in ivals:
            self[k] = v
        
    def __getitem__(self, key):
        if not key in self._data:
            raise KeyError(key)
        return self._data[key]

    def __setitem__(self, key, value):
        self._data.setdefault(key, []).append(value)

    def __delitem__(self, key):
        del self._data[key]
    
    def __len__(self):
        return len(self._data)

    def __iter__(self):
        return iter(key for key in self._data)
    

class KeyedListKeyType:
    def __init__(self, keystr):
        self.keystr = keystr


class KeyedListKey(KeyedListKeyType):
    def __call__(self, value):
        return value.get(self.keystr, Void)

    
class KeyedListAttr(KeyedListKeyType):
    def __call__(self, value):
        return getattr(value, self.keystr, Void)


class KeyedList(List[IterVal]):
    """Allow indexing a list of mappings by value(s) as well as index number
    
    Wraps a list of dictionary or other mapping types adding the ability to
    access list members by the value of a given key. For an example:

    >>> lst = [{'k': 'a', 'x': ''}, {'k': 'b', 'x': 8}, {'k': 'a', 'x': None}]
    >>> kl = KeyedList(lst, 'k')
    >>> kl.by_k['b']
    [{'k': 'b', 'x': 8}]
    >>> kl.by_k['a']
    [{'k': 'a', 'x': ''}, {'k': 'a', 'x': None}]
    
    Alternately, through __getitem__:
    >>> kl['k']['b']
    [{'k': 'b', 'x': 8}]
    """
    class KeyIdx(collections.abc.Mapping):
        def __init__(self, key, parent):
            self._parent = parent
            self.key = key
            
        def __getitem__(self, key):
            if isinstance(key, re.Pattern):
                return [ i for i in self._parent if key.match(self.key(i)) ]
            else:
                return [ i for i in self._parent if self.key(i) == key ]
        
        @property
        def __len__(self):
            return self._parent.__len__

        @property
        def __iter__(self):
            return self._parent.__iter__
    
    def __init__(self, data: Iterable[IterVal]=()) -> None:
        super().__init__(data)


    def __getattr__(self, attr):
        if attr.startswith('by_'):
            field, field = attr.split('_', 1)
            return KeyedList.KeyIdx(KeyedListAttr(field), self)
        else:
            return self.__getattribute__(attr)
"""                
    def __init__(self, data: Iterable[IterVal]=(), *,
                 keyclass: OneOrMany[KeyedListKeyType]) -> None:
        super().__init__(data)
        if isinstance(keyclass, KeyedListKeyType):
            kidx = self.KeyIdx(keyclass, self)
            setattr(self, f'by_{keyclass.keystr}', kidx)
            self._keys = [kidx]
        else:
            self._keys = []
            for key in keyclass:
                kidx = self.KeyIdx(key, self)
                self._keys.append(kidx)
                setattr(self, f'by_{key.keystr}', kidx)
"""
