.. role:: python(code)
	  :language: python
		     
hardehar package
================

This is very much a work in progress - with less progress than I'd like at the
moment. There is some additional analysis code I was working on that I'm not
ready to commit just yet, but the core capability - converting HAR JSON data
into a tree of Python dataclasses - is working. At least, it's working on the
single test case I tested it on.

Usage
-----

The simplest use case is something like:

.. code-block:: python

   from hardehar import har
   import pathlib

   har_file = pathlib.Path('/path/to/some/HAR/file.json')
   har_data = har.Har.load(har_file)


From there, :python:`dir(har_data)` should give you some idea of where to go. If you're
dealing with anything other than the most trivial of trivial HAR files, do
/not/ just :python:`print(har_data)`. The classes that form the data tree are
python :class:`~dataclasses.dataclass`\es, and have the default \__repr__ and
\__str__ functions. In short, you'll be watching class definitions scroll up
your terminal for eons.


Why for?
--------

No idea. I haven't decided if this is in any way useful vs. simply working
directly with the nested dictionaries returned by the JSON parser. You do get
"free" type conversion (e.g., timestamps are converted to
:class:`~datetime.datetime` objects, HTML is parsed into BeautifulSoup
documents, urls are pre-parsed, &c). But aside from that, the main benefit is
being able to reference a piece of data like:

.. code-block:: python

   har_data.element[0].request.method

instead of mucking around with braces.

In any case, if you find a use for it, or if you think it could be made useful
with the addition of some way radical feature, drop an issue in the issue
tracker.

Have fun, don't hurt anyone.


Submodules
----------

hardehar.har module
-------------------

.. automodule:: hardehar.har
   :members:
   :undoc-members:
   :show-inheritance:

hardehar.util module
--------------------

.. automodule:: hardehar.util
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: hardehar
   :members:
   :undoc-members:
   :show-inheritance:

