.. hardehar documentation master file, created by
   sphinx-quickstart on Sun Feb 28 18:16:14 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to hardehar's documentation!
====================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:
	     
   hardehar
   modules

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
