HARDEHAR
--------

Parses HAR data into a tree of Python dataclasses.

Installation
============

Requires Python 3.7 or later. Strongly recommend also installing
BeautifulSoup4 via pip or whatever. Usage without BS4 is "supported", but not
tested. At all.

If you want to rebuild the documentation because you hate yourself, you'll
need Sphinx and the sphinx-rtd-theme.

I recommend a virtual environment:

.. code:: 
	  
	  $ python -mvenv hardahar
	  $ cd hardehar
	  $ source bin/activate
	  $ mkdir src
	  $ cd src
	  $ git clone https://gitlab.com/breakslabs/hardehar/index.html
	  $ cd hardehar
	  $ python ./setup.py install

	  
Or like that.

Help Me!
========

See documentation at https://breakslabs.gitlab.io/hardehar

